/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Important notice:
 * Because the posix spawn API lacks support for setting terminal foreground
 * process group (tcsetpgrp), this shell is subject to race coditions.
 * For more information, check the following links:
 *
 * https://github.com/fish-shell/fish-shell/issues/3149
 * http://www.mail-archive.com/ast-developers@research.att.com/msg00719.html
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <spawn.h>
#include <simpleprint/simpleprint.h>

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define MAX_ARGS 16

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////

// Work strings
char terminal_currentdir[64];
char work[64];
int terminal_pos = 0;

// Arguments to be used for the process
char *pargs[MAX_ARGS];

// Environ is external
extern char** environ;

///////////////////////////////////////////////////////////////////////////////
// Wrappers
///////////////////////////////////////////////////////////////////////////////
void Delay (unsigned int time)
{
	struct timespec req;

	req.tv_sec = time / 1000;
	req.tv_nsec = (time % 1000) * 1000000;

	nanosleep (&req, 0);
}

///////////////////////////////////////////////////////////////////////////////
// Shell
///////////////////////////////////////////////////////////////////////////////
int strcmp_cmd (char *a, char *b)
{
	while (*a == *b)
	{
		if (*a == 0 && *b == 0)
			return 0;

		a++;
		b++;
	}

	if (*a == ' ' && *b == 0)
		return 0;

	return 1;
}

void cd (char **str)
{
	int ret = chdir (str[1]);

	if (ret < 0)
		SimplePrint ("Error setting directory\n");
	else if (!getcwd (terminal_currentdir, sizeof(terminal_currentdir)))
		SimplePrint ("Error getting directory\n");
}

void TerminalReset ()
{
	terminal_pos = 0;
	char setcolorb[] = {0x1B, 0x5B, '3', '4', ';', '1', 'm', 0};
	char resetcolor[] = {0x1B, 0x5B, '0', 'm', 0};
	SimplePrint (setcolorb);
	SimplePrint (terminal_currentdir);
	SimplePrint (" $ ");
	SimplePrint (resetcolor);
}

void ArgumentSetup (char *command)
{
	// Start with the first argument and tokenize command on the space char
	int pos = 0;

	do
	{
		char *tok = strtok (command, " ");
		pargs[pos] = tok;
		if (!tok)
			break;
		pos++;
		pargs[pos] = 0;
		command = 0;
	} while (pos < MAX_ARGS-1);
}

void ProcessCommand (char *str)
{
	// Argument setup, splits str into what will become argv
	ArgumentSetup (str);

	// Check for built-ins first
	if (!strcmp_cmd (pargs[0], "cd"))
		cd (pargs);
	else
	{
		// Set spawn attributes
		posix_spawnattr_t attr;
		posix_spawnattr_init (&attr);
		posix_spawnattr_setflags(&attr, POSIX_SPAWN_SETPGROUP);
		posix_spawnattr_setpgroup (&attr, 0);

		// Launch process!
		pid_t pid;
		int ret = posix_spawn (&pid, pargs[0], 0, &attr, pargs, environ);

		// Delete attr
		posix_spawnattr_destroy (&attr);

		// Check for errors or wait for it to complete
		if (ret != 0 || pid < 0)
			PrintError ("ush", pargs[0]);
		else
		{
			int status;
			waitpid (pid, &status, 0);
		}
	}
}

int main (int argc, char **argv)
{
	getcwd (terminal_currentdir, sizeof(terminal_currentdir));
	TerminalReset ();

	while (1)
	{
		char recvchar;
		int ret = read (0, &recvchar, 1);

		if (ret != 1)
		{
			Delay (5);
			continue;
		}

		char echo[] = {recvchar, 0};

		//printf ("character %.2x\n", recvchar);

		if (recvchar == '\r' || recvchar == '\n')
		{
			SimplePrint ("\n");
			work[terminal_pos] = 0;

			if (terminal_pos)
				ProcessCommand (work);

			TerminalReset ();
		}
		else if (recvchar == 0x08)
		{
			// Backspace at the end
			if (terminal_pos)
			{
				char str[] = {0x08, ' ', 0x08, 0};
				SimplePrint (str);
				terminal_pos--;
			}
		}
		else
		{
			// Append the character
			terminal_pos++;

			if (terminal_pos == 64)
				TerminalReset ();
			else
			{
				work[terminal_pos-1] = recvchar;
				SimplePrint (echo);
			}
		}
	}

	return 0;
}
