# Readme #

This is the most basic shell available. It is only useful for starting other
programs in the most simple way.

If you want to use as little memory as possible for a shell, using this is
the most appropriate.